import 'package:flutter/material.dart';
import 'package:frontend/components/menuItem.dart';
import 'package:frontend/constants.dart';

/// Левая навигационная панель
class LeftNavBar extends StatefulWidget {
  LeftNavBar({Key? key, required this.onClick}) : super(key: key);
  final void Function(int id) onClick;

  @override
  _LeftNavBarState createState() => _LeftNavBarState();
}

class _LeftNavBarState extends State<LeftNavBar> {
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    return Container(
      width: 250,
      height: height,
      decoration: BoxDecoration(
        boxShadow: [BoxShadow(color: shadow, blurRadius: 15)],
        borderRadius: BorderRadius.only(topRight: Radius.circular(40)),
        color: white,
      ),
      padding: EdgeInsets.only(left: 40, top: 80),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          for (int i = 0; i < 7; i++)
            InkWell(
              child: MenuItes(index: i),
              onTap: () => widget.onClick(i),
            ),
        ],
      ),
    );
  }
}
