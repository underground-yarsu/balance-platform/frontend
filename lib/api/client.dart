import 'package:frontend/api/schema/access_token.dart';
import 'package:frontend/api/schema/achieve_team.dart';
import 'package:frontend/api/schema/achieve_user.dart';
import 'package:frontend/api/schema/achievement.dart';
import 'package:frontend/api/schema/login_request.dart';
import 'package:frontend/api/schema/profile.dart';
import 'package:frontend/api/schema/put_profile_request.dart';
import 'package:frontend/api/schema/reg_request.dart';
import 'package:frontend/api/schema/team_balance_response.dart';
import 'package:frontend/api/schema/team_info.dart';
import 'package:frontend/api/schema/user.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';

part 'client.g.dart';

// use this command to update generated files:
// flutter pub run build_runner build

@RestApi(baseUrl: 'https://back-hack.herokuapp.com')
abstract class ApiClient {
  factory ApiClient(Dio dio, {String baseUrl}) = _ApiClient;

  /// Store JWT like this:
  ///
  /// ```
  /// Prefs.jwt = client.reg().accessToken;
  /// // or
  /// Prefs.jwt = client.login().accessToken;
  ///
  /// // unlogin
  /// Prefs.jwt = null;
  /// ```
  @POST('/reg')
  Future<AccessToken> reg(
    @Body() RegRequest regRequest,
  );

  /// Store JWT like this:
  ///
  /// ```
  /// Prefs.jwt = client.reg().accessToken;
  /// // or
  /// Prefs.jwt = client.login().accessToken;
  ///
  /// // unlogin
  /// Prefs.jwt = null;
  /// ```
  @POST('/login')
  Future<AccessToken> login(
    @Body() LoginRequest loginRequest,
  );

  @PUT('/profile-switch')
  Future<void> putProfile(
    @Body() PutProfileRequest putProfileRequest,
  );

  @GET("/profile/{telegram_name}")
  Future<Profile> getProfile(
    @Path('telegram_name') String telegramName,
  );

  @GET('/all_achieves')
  Future<List<Achievement>> allAchieves();

  @GET('/team_balance/{id_team}')
  Future<TeamBalanceResponse> teamBalance(@Path('id_team') int idTeam);

  @GET('/team_info/{id_team}')
  Future<TeamInfo> teamInfo(@Path('id_team') int idTeam);

  @PUT('/team_enter/{id_team}')
  Future<void> teamEnter(@Path('id_team') int idTeam);

  @GET('/team_list')
  Future<List<TeamInfo>> teamList();

  @GET('/user_list')
  Future<List<User>> userList();

  @GET('/list_achieve_users')
  Future<List<AchieveUser>> listAchieveUsers();

  @GET('/list_achieve_teams')
  Future<List<AchieveTeam>> listAchieveTeams();

  @GET("/get-achieve/{telegram_name}")
  Future<List<Achievement>> getAchieve(
    @Path('telegram_name') String telegramName,
  );

  @PUT('/upd_status/{status}')
  Future<void> updStatus(
    @Path('status') int status,
  );
}

late ApiClient client;
