import 'package:flutter/material.dart';
import 'package:frontend/constants.dart';
import 'package:frontend/page/rating/myRatingCard.dart';

class RatingPage extends StatefulWidget {
  @override
  _RatingPageState createState() => _RatingPageState();
}

class _RatingPageState extends State<RatingPage> {
  /// Вкладка рейтинг
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Container(
      height: height,
      padding: EdgeInsets.only(top: 100, left: 120, right: 120),
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Мой рейтинг',
              style: TextStyle(color: blueText, fontSize: 28),
            ),
            MyRating(),
            Text(
              'Общий рейтинг',
              style: TextStyle(color: blueText, fontSize: 28),
            ),
          ],
        ),
      ),
    );
  }
}
