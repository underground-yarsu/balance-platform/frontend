import 'dart:ui';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:frontend/api/client.dart';
import 'package:frontend/api/schema/reg_request.dart';
import 'package:frontend/components/authBack.dart';
import 'package:frontend/global.dart';
import 'package:frontend/page/auth/questions/questions.dart';
import 'package:frontend/components/buttons.dart';
import 'package:frontend/components/text_field.dart';
import 'package:frontend/page/profileView.dart';
import '../../constants.dart';
import 'login.dart';

class Registration extends StatefulWidget {
  @override
  _RegistrationState createState() => _RegistrationState();
}

class _RegistrationState extends State<Registration>
    with SingleTickerProviderStateMixin {
  bool canGetName = false, canGetTelegram = false, canGetPassword = false;
  String errorName = "", errorTelegram = "", errorPassword = "";

  TextEditingController email = TextEditingController(),
      password = TextEditingController(),
      telegram = TextEditingController();

  void checkCanGetPassword(String value) {
    if (value.isNotEmpty) {
      setState(() {
        canGetPassword = true;
        canGetName = true;
      });
    } else {
      setState(() {
        canGetPassword = false;
        canGetName = false;
      });
    }
  }

  Future<void> register() async {
    try {
      var res = await client.reg(
        RegRequest(
          fio: this.email.text,
          telegramName: this.telegram.text,
          password: this.password.text,
        ),
      );
      Prefs.jwt = res.accessToken;
      Prefs.telegram = this.telegram.text;
      Navigator.push<void>(
        context,
        MaterialPageRoute<void>(
          builder: (BuildContext context) => StartQuestions(),
        ),
      );
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AuthPage(
        width: 670,
        height: 550,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: MediaQuery.of(context).size.height * 0.05),
            Text(
              "Привет,\n приятно познакомиться!",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontStyle: FontStyle.normal,
                fontSize: 32,
                color: text,
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.02),
            Text(
              "Пожалуйста, укажите следующие детали\nдля Вашей новой учетной записи ",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontStyle: FontStyle.normal,
                fontSize: 14,
                color: semiText,
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.05,
            ),
            Container(
              width: 300,
              child: Column(
                children: [
                  MyTextField(
                    txt: "Имя пользователя",
                    controller: email,
                    onChanged: (value) => checkCanGetPassword(value),
                    hintText: "Имя и Фамилия",
                    errorText: errorName,
                  ),
                  MyTextField(
                    controller: telegram,
                    onChanged: (value) => checkCanGetPassword(value),
                    hintText: "Telegram",
                    errorText: errorTelegram,
                    isPassword: true,
                  ),
                  MyTextField(
                    controller: password,
                    onChanged: (value) => checkCanGetPassword(value),
                    hintText: "Пароль",
                    errorText: errorPassword,
                    isPassword: true,
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.01,
                  ),
                  MyButton(
                    text: 'Продолжить',
                    onTap: () {
                      bool canSignUp = true;
                      if (canGetName) {
                        setState(() => errorName = "");
                      } else {
                        setState(() {
                          errorName = "Пустое поле недопустимо";
                          canSignUp = false;
                        });
                      }
                      if (canGetPassword) {
                        setState(() => errorPassword = "");
                      } else {
                        setState(() {
                          errorPassword = "Не указан пароль";
                          canSignUp = false;
                        });
                      }
                      if (canSignUp) {
                        register();
                      }
                    },
                  ),
                ],
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.05),
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                style: TextStyle(
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                  color: text,
                ),
                children: <TextSpan>[
                  TextSpan(text: 'Уже есть аккаунт? ', style: TextStyle()),
                  TextSpan(
                    recognizer: TapGestureRecognizer()
                      ..onTap = () => Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => LogIn(),
                            ),
                          ),
                    text: 'Войти',
                    style: TextStyle(color: btnText),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
