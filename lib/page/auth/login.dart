import 'dart:ui';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:frontend/api/client.dart';
import 'package:frontend/api/schema/login_request.dart';
import 'package:frontend/api/schema/profile.dart';
import 'package:frontend/components/authBack.dart';
import 'package:frontend/global.dart';
import 'package:frontend/page/profileView.dart';
import 'package:websafe_svg/websafe_svg.dart';
import 'package:frontend/components/buttons.dart';
import 'package:frontend/components/text_field.dart';
import '../../constants.dart';
import 'register.dart';

class LogIn extends StatefulWidget {
  @override
  _LogInState createState() => _LogInState();
}

class _LogInState extends State<LogIn> with SingleTickerProviderStateMixin {
  bool canGetName = false, canGetPassword = false;
  String errorName = "", errorPassword = "";

  TextEditingController email = TextEditingController(),
      password = TextEditingController();

  void checkCanGetPassword(String value) {
    if (value.isNotEmpty) {
      setState(() {
        canGetPassword = true;
        canGetName = true;
      });
    } else {
      setState(() {
        canGetPassword = false;
        canGetName = false;
      });
    }
  }

  Future<void> signUp() async {
    try {
      var res = await client.login(
        LoginRequest(
          telegramName: this.email.text,
          password: this.password.text,
        ),
      );
      Prefs.jwt = res.accessToken;
      Prefs.telegram = this.email.text;
      print("ok");
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) => ProfileView(),
        ),
        (route) => false,
      );
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    if (Prefs.jwt != null) {
      client.getProfile(Prefs.telegram!).then((Profile value) {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => ProfileView(),
          ),
          (route) => false,
        );
      }, onError: (Object o, StackTrace s) {
        print(o);
        print(s);
        Prefs.jwt = null;
        Prefs.telegram = null;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AuthPage(
        width: 700,
        height: 450,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.06,
            ),
            Text(
              "Привет, рады тебя видеть!",
              style: TextStyle(
                fontStyle: FontStyle.normal,
                fontSize: 32,
                color: text,
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.02,
            ),
            Text(
              "Войдите, чтобы продолжить",
              style: TextStyle(
                fontStyle: FontStyle.normal,
                fontSize: 14,
                color: semiText,
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.03),
            Container(
              width: 300,
              child: Column(
                children: [
                  MyTextField(
                    controller: email,
                    onChanged: (value) => checkCanGetPassword(value),
                    hintText: "Логин",
                    errorText: errorName,
                  ),
                  MyTextField(
                      controller: password,
                      onChanged: (value) => checkCanGetPassword(value),
                      hintText: "Пароль",
                      errorText: errorPassword,
                      isPassword: true),
                  MyButton(
                    text: 'Войти',
                    onTap: () {
                      bool canSignUp = true;
                      if (canGetName) {
                        setState(() => errorName = "");
                      } else {
                        setState(() {
                          errorName = "Пустое поле недопустимо";
                          canSignUp = false;
                        });
                      }
                      if (canGetPassword) {
                        setState(() => errorPassword = "");
                      } else {
                        setState(() {
                          errorPassword = "Не указан пароль";
                          canSignUp = false;
                        });
                      }
                      if (canSignUp) {
                        signUp();
                      }
                    },
                  ),
                ],
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.03),
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                style: TextStyle(
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w500,
                    fontSize: 16,
                    color: text),
                children: <TextSpan>[
                  TextSpan(
                      recognizer: TapGestureRecognizer()
                        ..onTap = () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Container())), //
                      text: 'Забыли пароль?',
                      style: TextStyle(color: btnText))
                ],
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.02),
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                style: TextStyle(
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w500,
                    fontSize: 16,
                    color: text),
                children: <TextSpan>[
                  TextSpan(text: 'Нет аккаунта? ', style: TextStyle()),
                  TextSpan(
                    recognizer: TapGestureRecognizer()
                      ..onTap = () => Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => Registration(),
                            ),
                          ),
                    text: 'Создать аккаунт',
                    style: TextStyle(color: btnText),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
