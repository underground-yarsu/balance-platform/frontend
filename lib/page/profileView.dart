import 'package:flutter/material.dart';
import 'package:frontend/components/profileAppBar.dart';
import 'package:frontend/components/rightBar.dart';
import 'package:frontend/constants.dart';
import 'package:frontend/page/bets/bets.dart';
import 'package:frontend/page/map/map.dart';
import 'package:frontend/page/rating/rating.dart';
import 'package:frontend/page/shop/shop.dart';
import 'package:frontend/page/stats.dart';
import 'package:frontend/page/task/task.dart';
import 'package:websafe_svg/websafe_svg.dart';

/// Личный кабинет
class ProfileView extends StatefulWidget {
  @override
  _ProfileViewState createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  String backgroundPath = 'assets/backgrounds/',
      background = 'task_background.png';
  int _id = 0;

  /// Меняем экран нажатием на строку навигационной панели
  Widget _buildBody() {
    switch (_id) {
      case 0:
        background = 'task_background.png';
        return TaskPage();
      case 1:
        return MapPage();
      case 2:
        return StatsPage();
      case 3:
        return Placeholder();
      case 4:
        background = 'rating_background.png';
        return RatingPage();
      case 5:
        background = 'bets_background.png';
        return BetsPage();
      case 6:
        background = 'rating_background.png';
        return ShopPage();
      default:
        background = 'rating_background.png';
        return Placeholder();
    }
  }

  final menuItemsIcons = [
    'tasks',
    'map',
    'statistic',
    'tips',
    'rating',
    'bets',
    'shop',
  ];
  final menuItems = [
    'Задачи',
    'Карта',
    'Статистика',
    'Советы',
    'Рейтинг',
    'Ставки',
    'Магазин',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: profileBackground,
        body: Container(
          width: MediaQuery.of(context).size.width,
          child: Stack(
            children: [
              Row(
                children: [
                  Expanded(
                    child: Stack(
                      children: [
                        Align(
                            alignment: Alignment.centerRight,
                            child: Image.asset(backgroundPath + background,
                                fit: BoxFit.fill)),
                        Row(children: [
                          Container(
                            width: 250,
                            height: MediaQuery.of(context).size.height,
                            decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(color: shadow, blurRadius: 15)
                              ],
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(40)),
                              color: white,
                            ),
                            padding: EdgeInsets.only(top: 88),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                for (int i = 0; i < 7; i++)
                                  InkWell(
                                    child: Container(
                                      color: _id == i
                                          ? Color(0xfff1f0f5)
                                          : Colors.transparent,
                                      padding: EdgeInsets.only(
                                          left: 40, top: 15, bottom: 13),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            width: 30,
                                            height: 30,
                                            child: WebsafeSvg.asset(
                                                'icons/menuItem/${menuItemsIcons[i]}.svg'),
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(left: 20),
                                            child: Text(
                                              menuItems[i],
                                              style: TextStyle(
                                                  color: blueText,
                                                  fontSize: 18),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    onTap: () {
                                      setState(() {
                                        _id = i;
                                      });
                                    },
                                  ),
                              ],
                            ),
                          ),
                          Expanded(child: _buildBody()),
                        ])
                      ],
                    ),
                  ),
                  if (_id == 5) RightBar()
                ],
              ),
              Positioned(top: 0, right: 0, child: ProfileAppBar())
            ],
          ),
        ));
  }
}
