import 'package:flutter/material.dart';
import 'package:frontend/components/rightBar.dart';
import 'package:frontend/page/bets/card.dart';

class BetsPage extends StatefulWidget {
  @override
  _BetsPageState createState() => _BetsPageState();
}

class _BetsPageState extends State<BetsPage> {

  @override
  void initState() {
    
    super.initState();
  }
  /// Вкладка Ставки
  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width - 250,
      child: Stack(
        children: [
          Image.asset(
            "assets/backgrounds/rids_background.png",
            fit: BoxFit.cover,
          ),
          Container(
            height: MediaQuery.of(context).size.height,
            alignment: Alignment.center,
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                children: [
                  for (int i = 1; i < 11; i++)
                    ChallengeCard(
                      commandName: 'Храни',
                      avatar: i.toString(),
                    ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
