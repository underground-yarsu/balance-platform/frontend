import 'package:flutter/material.dart';
import 'package:frontend/components/battleDialog.dart';
import 'package:frontend/components/buttons.dart';
import 'package:frontend/constants.dart';

/// Карточка соревнования
class ChallengeCard extends StatelessWidget {
  final String commandName;
  final String avatar;
  ChallengeCard({required this.commandName, required this.avatar});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 15, left: 20, right: 20),
      width: 550,
      height: 100,
      padding: EdgeInsets.symmetric(horizontal: 30),
      decoration: BoxDecoration(
          color: white,
          borderRadius: BorderRadius.circular(30),
          boxShadow: [BoxShadow(color: shadow, blurRadius: 15)]
      ),
      child: Row(
        children: [
          Image.asset('avatars/$avatar.png'),
          SizedBox(width: 20),
          Text(
            commandName,
            style: TextStyle(color: blueText, fontSize: 18),
          ),
          Spacer(),
          MyOutlinedButton(
              onTap: () => showDialog(
                  context: context,
                  builder: (BuildContext context) => BattleDialog(context)),
              text: 'Бросить вызов'
          )
        ],
      ),
    );
  }
}
