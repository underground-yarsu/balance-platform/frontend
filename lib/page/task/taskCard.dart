import 'package:flutter/material.dart';
import 'package:frontend/constants.dart';
import 'package:websafe_svg/websafe_svg.dart';

class MyTasks extends StatefulWidget {
  @override
  _MyTasksState createState() => _MyTasksState();
}

class _MyTasksState extends State<MyTasks> {
  /// Карточка Задания пользователя
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 310,
      decoration: BoxDecoration(
        color: white,
        borderRadius: BorderRadius.circular(30),
        boxShadow: [
          BoxShadow(color: shadow, blurRadius: 15),
        ],
      ),
      child: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(30),
        child: Stack(
          alignment: Alignment.center,
          children: [
            getPanel(780, Color(0xFFF0F1FE), 0),
            getPanel(640, Color(0xFFFDFDF1), 200),
            getPanel(570, Color(0xFFFDF0FB), 400),
            Positioned(
              right: 0,
              child: Container(
                alignment: Alignment.centerLeft,
                child: Row(
                  children: [
                    WebsafeSvg.asset(
                      'images/cards/lvl_up.svg',
                      width: 200,
                      height: 200,
                    ),
                    Container(
                      width: 300,
                      height: 220,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                            'Машина - убийца',
                            style: TextStyle(
                                color: Color(0xFFFF6C98), fontSize: 18),
                          ),
                          Text(
                            'Выполнил много заданий?\nДумаешь, что уже можешь осилить все?\nЧто ж, тогда следующее задание для тебя! Нужно верифицировать 10 заявок 2-НДФЛ.\nЗадание не простое, но и награда будет соответствующей!',
                            softWrap: true,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Color(0x88FF6C98), fontSize: 14),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getPanel(double width, Color color, double left) => Positioned(
        left: left,
        right: 0,
        child: Container(
          alignment: Alignment.centerRight,
          height: 230,
          width: width,
          decoration: BoxDecoration(
            color: color,
            borderRadius: BorderRadius.circular(30),
            boxShadow: [
              BoxShadow(color: shadow, blurRadius: 10),
            ],
          ),
        ),
      );
}
